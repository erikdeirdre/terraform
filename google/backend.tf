terraform {
  backend "gcs" {
    bucket      = "beeweb-terraform-state"
    prefix      = "gitlab"
    credentials = "github_credential.json"
  }
}